'use strict'

const { json, send } = require('micro')

const pkg = require('../package.json')
const tmpdir = require('../lib/tmpdir')
const git = require('../lib/git')

const AUTHORIZATION_HEADER = 'bk5A3ZgzkPKWcbFD1wmL'
const MASTER_BRANCH = 'refs/heads/master'

module.exports = async (req, res) => {

  // return from pings
  if (req.method === 'GET') {
    return `Online v${pkg.version}`
  }

  // ensure this request came with the right key
  if (req.headers['x-gitlab-token'] !== AUTHORIZATION_HEADER) {
    return send(res, 401, 'Invalid authentication header')
  }

  // parse the body
  const body = await json(req)

  // log what we're working on
  console.log('Processing new commit "%s" on project "%s"...', body.checkout_sha, body.project.name)
  console.log('Details: %s', JSON.stringify(pretty(body), undefined, 2))

  // ensure that the event_name is "push"
  if (body.event_name !== 'push') {
    console.log(`git event not 'push', bailing!`)
    return 'OK'
  }

  // ensure we have a git URL
  if (!body.repository.git_ssh_url) {
    console.log(`git event did not contain an SSH URL to use for cloning`)
    return 'OK'
  }

  // create a temp directory
  const targetPath = await tmpdir.create()

  // clone the changed repo to that directory
  await git.clone(targetPath, body.repository.git_ssh_url)

  // install the update
  await git.install(targetPath, getBranch(body))

  // clean up the temporary directory
  await tmpdir.remove(targetPath)

  // finished working
  console.log('Finished processing commit "%s" on project "%s"', body.checkout_sha, body.project.name)
  return 'OK'
}

function getBranch(change) {
  const ref = change.ref
  const split = ref.split('/')
  return split[split.length - 1]
}

function pretty(change) {
  return {
    user: change.user_email,
    branch: getBranch(change),
    commits: change.commits.map(commit => {
      return {
        id: commit.id,
        message: commit.message
      }
    }),
    sha: {
      before: change.before,
      after: change.after
    },
    project: {
      name: change.project.name,
      url: change.project.git_ssh_url
    }
  }
}