'use strict'

const os = require('os')
const path = require('path')
const fs = require('mz/fs')
const rimraf = require('rimraf')
const shortid = require('shortid')

const tmp = os.tmpdir()

exports.create = async () => {
  console.log('OS tmp directory: %s', tmp)
  const target = path.join(tmp, shortid.generate())
  await fs.mkdir(target)
  console.log('Created temporary directory "%s"', target)
  return target
}

exports.remove = async target => {
  if (await fs.exists(target)) {
    console.log('Removing temporary directory "%s"', target)
    await remove(target)
  }
}

async function remove(target) {
  await new Promise((resolve, reject) => {
    rimraf(target, err => err ? reject(err) : resolve())
  })
}