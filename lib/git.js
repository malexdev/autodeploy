'use strict'

const path = require('path')
const fs = require('mz/fs')
const exec = require('mz/child_process').exec

exports.clone = async (directory, repository) => {
  console.log('Cloning "%s" into "%s"', repository, directory)
  await exec(`git clone ${repository} ${directory}`)
}

// check the directory for whether it has a `autodeploy.sh` or `autodeploy.js` file.
// if both, prefer `.js`.
exports.install = async (directory, branch) => {
  const shPath = path.join(directory, autodeployScript(branch))
  const shExists = await fs.exists(shPath)

  if (shExists) {
    console.log(`Shell installer found for branch ${branch}, running "sh %s"`, shPath)
    await exec(`sh ${shPath}`, { cwd: directory })
    console.log('Installation finished')
  } else {
    console.log('No installer found, no work performed.')
  }
}

function autodeployScript(branch) {
  if (branch === 'master') { return `autodeploy.sh` }
  return `autodeploy-${branch}.sh`
}