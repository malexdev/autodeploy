#!/bin/bash

mv /root/node/autodeploy /root/node/autodeploy-backup
mkdir /root/node/autodeploy
cp -r ./* /root/node/autodeploy/
cd /root/node/autodeploy
yarn
pm2 restart autodeploy